from player import Player

class WeightedPlayer(Player):
    """
    A Player who cooperates with probability equal to an opponent's
    reputation.

    """

    def __init__(self, rand):
        """
        Constructs a new WeightedPlayer using `rand' as the random
        number generator.

        """

        super(WeightedPlayer, self).__init__()

        self.rand = rand

    def _hunt_choices(self, round_number, current_food, current_reputation, m,
                      player_reputations):
        return [Player.HUNT if self.rand.random() <= r else Player.SLACK
                for r in player_reputations]
