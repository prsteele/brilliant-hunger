from player import Player
import random

class ThresholdPlayer(Player):
    """
    A ThresholdPlayer will always cooperate with players who have a
    reputation above some threshold, and always defect against all
    others.

    """


    def __init__(self, threshold):
        """
        Constructs a new ThresholdPlayer, using `threshold' as the
        cooperation threshold.


        """

        self.threshold = threshold

        super(ThresholdPlayer, self).__init__()

    def _hunt_choices(self, round_number, current_food, current_reputation, m,
                      player_reputations):

        return [Player.HUNT if r >= self.threshold else Player.SLACK
                for r in player_reputations]

    def __str__(self):
        """
        Return a string representation of the Player.

        """

        return "ThresholdPlayer(%.2f): %i, %.2f" % \
            (self.threshold, self.current_food, self.current_reputation)
