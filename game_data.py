class GameData(object):
    """
    This class collects stores data about each round of the game.

    """

    def __init__(self, initial_population):
        """
        Construct a new GameData object.

        """

        self.initial_population = initial_population

        # A list of the communal awards in each round
        self.awards = []

        # A list of the number of hunts performed in each round
        self.hunts = []

        # A list of the number of slacks performed in each round
        self.slacks = []

        # A list of the population of players at the start of each
        # round
        self.population = []

    def food_bounds(self, reputation):
        """
        Computes bounds on the amount of food that a player with the
        given reputation can have.

        Arguments:
        
        reputation The reputation of the player

        Returns:

        A tuple of the (lower bound, upper bound) estimates on the
        player's food reserves

        """

        # Compute the total number of excursions a living player has
        # been part of
        total_excursions = sum(self.population) - len(self.population)

        total_hunts = reputation * total_excursions
        total_slacks = total_excursions - total_hunts

        starting_food = self.initial_population * (self.initial_population - 1)

        total_award = sum(self.award)
        
        ub = starting_food + total_award + total_slacks
        lb = max(0, starting_food + total_award - 3 * total_hunts)

        return (lb, ub)
