from __future__ import division

import random

from tournament import Tournament
from always_defect_player import AlwaysDefectPlayer
from always_cooperate_player import AlwaysCooperatePlayer
from random_player import RandomPlayer
from weighted_player import WeightedPlayer
from reputable_player import ReputablePlayer
from threshold_player import ThresholdPlayer
from adaptive_threshold_player import AdaptiveThresholdPlayer

M = 5000
p = .01
seed = 1
rand = random.Random(seed)

players = []

nplayers = 10

for i in xrange(0, nplayers):
    players.append(AlwaysDefectPlayer())

for i in xrange(0, nplayers):
    players.append(AlwaysCooperatePlayer())

for i in xrange(0, nplayers):
    players.append(RandomPlayer((i + 1) / nplayers, rand))

for i in xrange(0, nplayers):
    players.append(WeightedPlayer(rand))

for i in xrange(0, nplayers):
    players.append(ReputablePlayer(rand))

for i in xrange(0, nplayers):
    players.append(ThresholdPlayer((i + 1) / (nplayers)))

for i in xrange(0, nplayers):
    players.append(AdaptiveThresholdPlayer(rand, (i + 1) / (nplayers), (i + 1) / (nplayers)))

tournament = Tournament(players, M, p, rand)
tournament.play()

def compare_players(p, q):
    """ Sort primarily on survival, and break ties by food """
    
    if p.age < q.age:
        return -1
    elif p.age > q.age:
        return 1
    else:
        if p.current_food < q.current_food:
            return -1
        elif p.current_food > q.current_food:
            return 1
        else:
            return 0

players.sort(cmp=compare_players, reverse=True)

for p in players:
    print p
print tournament.round
print tournament.awards
