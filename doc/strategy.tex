\documentclass{article}

\usepackage{prsteele}
\usepackage{url}
\usepackage{fullpage}
\usepackage{listings}

\title{Brilliant Strategies}
\author{Patrick Steele}
\date{\today}

\begin{document}

\maketitle

\noindent We consider the problem of winning the game described at
\url{brilliant.org/competitions/hunger-games}. In
section~\ref{sec:gameplay} we review the rules of the game. In
section~\ref{sec:strategies}, we compare a number of potential
strategies. Finally, in section~\ref{sec:code} we finalize our
submitted strategy, list our submitted code, and discuss how we
arrived at our solution.

\section{Gameplay}
\label{sec:gameplay}

The game consists of an unknown number of rounds during which players
compete for a single resource, food. In each round all players compete
with all other players in a 2-person non-cooperative game; players are
not informed of the identity of their opponents but are given the
reputation of their opponents, a measure of the number of times an
opponent has cooperated.. At the end of the round food is awarded to
players based on performance, and any players with zero or less food
are eliminated. The game ends when all rounds have been played or one
or zero players remain. Players are free to remember information from
round to round, but are not directly informed of the identity of their
opponents. In the sections below we elaborate on these topics; full
rules are available at
\url{https://brilliant.org/competitions/hunger-games/rules/}

\subsection{Rounds}

The game consists of an unknown number of rounds $M$. In particular,
the game will run for $M' + z$ rounds, where $M'$ is a
constant and $z$ is geometrically distributed with an unknown mean;
since both $M'$ and $z$ are unknown, we assume that the game will run
for some fixed, but unknown, number of rounds $M$. In a $P$-player
game, players begin with $300 (P - 1)$ food.

\subsection{Winning}

There are two victory conditions in this game: winning and
surviving. A player wins by having the most food at the end of the
game, or in the case of a tie having the higher reputation; note that
if only a single player survives, then this player necessarily wins. A
player survives by failing to run out of food; note that a player may
win without surviving, which is realized in the case where all
remaining players run out of food in the same round, thus halting the
game. Although players may choose strategies to maximize the chances
of surviving, we seek a strategy that will win, not necessarily while
surviving.

\subsection{Games}

In each round all pairs of players compete in a 2-person
non-cooperative game with the payoff matrix shown in
figure~\ref{fig:payoff}, where each player must choose to hunt or
slack. In particular, at the start of a round with $P > 1$ players a
player is given a vector of the $P - 1$ reputation values (discussed
below) of her opponents in some order, and the player must respond
with a vector of $P - 1$ \textit{hunt} or \textit{slack} values,
corresponding to choices in the game; the order is not revealed to the
player. At the end of the round players are informed of the outcome of
each game, and food is awarded accordingly. Additionally, at the start
of the round players are informed of some integer $m$ chosen uniformly
at random over the integers $\cbrace{1, \ldots, P (P - 1) - 1}$; if
the total number of hunt decisions is $m$ or greater, every player is
awarded an additional $P (P - 1)$ food. Whether the extra food is
awarded or not players are informed of the total number of hunts each
round.

\subsection{Reputation}

As discussed in the section above, players must decide the actions of
each game without being told who each opponent is; rather, they know
only the reputation of that opponent. If an opponent has hunted $H$
times and slacked $S$ times, her reputation is $H / (S + H)$; thus,
reputation is a measure of the cooperativeness of an opponent. Since
only the reputation of opponents is provided to a player, in general
the player cannot perfectly track the decisions of individual
opponents.

\subsection{Tracked information}

As already mentioned, players are not informed of the identity of
their opponents explicitly. However, players may record any
information provided and use this information as they see fit. The
information provided amounts to the number of rounds played, the
number of hunts performed in each round, the reputation of all
opponents in each round, the outcome of each hunt, the number of
players in each round, the amount of food the player has at the start
of each round, and the total amount of bonus food dispersed each
round.

\begin{figure}[htp]
  \centering
  
  \begin{minipage}{1.5in}
    \begin{equation*}
      \begin{array}{r | c c}
        & H & S \\
        \hline
        H & 0 & -3 \\
        S & 1 & -2
      \end{array}
    \end{equation*}
  \end{minipage}
  \begin{minipage}{1.5in}
    \begin{equation*}
      \begin{array}{r | c c}
        & H & S \\
        \hline
        H & 2 & -1 \\
        S & 3 & 0
      \end{array}
    \end{equation*}
  \end{minipage}

  \caption{Left: the payoff matrix when no common goods are
    distributed. Right: the payoff matrix when common goods are
    distributed. In both cases the player in consideration chooses a
    row, while her opponent chooses the column. In the case where
    common goods are distributed, we amortize the common goods across
    all games, yielding 2~food per game. ``H'' denotes the choice to
    hunt, while ``S'' denotes the choice to slack.}
  \label{fig:payoff}
\end{figure}

\section{Strategies}
\label{sec:strategies}

We now introduce and analyze candidate strategies for competing in the
game.

\subsection{Always slack}

Possibly the simplest strategy for a player to use is to always
slack. From the payoff matrix in figure~\ref{fig:payoff}, we see that
this strategy will pay out 1~if an opponent hunts, and -2~if an
opponent slacks; this is a strictly favorable outcome compared to
hunting, which has a payout of 0~if the opponent hunts and $-3$~if the
opponent slacks. In fact, for a single iteration slacking achieves a
Nash equilibrium.

In general, always slacking is not a winning strategy. In particular,
if $P - 2$ players choose to always slack and 2~players adopt a
strategy of slacking against zero reputation players and cooperating
otherwise, all slacking players will eventually run out of food with
high probability, provided the game goes on long
enough.\footnote{Again, it could be that the game is forced to end
  after only a single round --- however, we assume that $M$ is large
  enough to allow for interesting outcomes.}

\begin{proof}
  Suppose there are $P - 2 \ge 1$ players who always slack, and 2~player
  that always slack against zero reputation players and always
  cooperate otherwise; as an edge case, these 2~players cooperate with
  everyone in the first round.

  After the first round, all slacking players receive a payoff of $-2
  (P - 3) + 2$ food from the games, since only two players hunt with
  them. The 2~cooperating players receive a payout of $-3 (P - 2) - 0
  \cdot 1$, since $P - 2$ players slack against them and one hunts
  with them. After the first round, the cooperating players can
  correctly identify slackers, and so receive a payoff of $-2 (P - 2)
  - 0 \cdot 1$ each round, as they slack against slackers and
  cooperate with each other. Slackers, however, receive a payoff of
  $-2 (P - 1)$, since all opponents slack with them. Thus, ignoring
  the communal award, the food of a slacking player at the end of
  round $n$, $\tilde{f}_s(n)$, is
  \begin{equation*}
    \tilde{f}_s(n) =
    \begin{cases}
      300 (P - 1) - 2 (P - 3) + 2, & n = 1 \\
      300 (P - 1) - 2 (P - 3) + 2 - 2 (P - 1) (n - 1), & n \ge 2,
    \end{cases}
  \end{equation*}
  while the food of a cooperating player at the end of round $n$,
  $\tilde{f}_c(n)$, is
  \begin{equation*}
    \tilde{f}_c(n) =
    \begin{cases}
      300 (P - 1) - 3 (P - 2), & n = 1 \\
      300 (P - 1) - 3 (P - 2) - 2 (P - 2) (n - 1), & n \ge 2.
    \end{cases}
  \end{equation*}
  Additionally, there is the communal award of food. Since player
  strategies are fixed, we know precisely how many hunts will be
  undertaken each round; specifically, there will be $2 (P - 1)$ in
  the first round and $2$ in all remaining rounds. Since the
  cooperation threshold $m$ is chosen uniformly at random over the
  integers $1, \ldots, P (P - 1) - 1$, we can express the food of
  slackers and cooperating players at the end of round $n$ as $f_s(n)$
  and $f_c(n)$, respectively, where
  \begin{equation*}
    f_s(n) = \tilde{f}_s(n) + z_1 + \sum_{i = 2}^n z_i, \qquad 
    f_c(n) = \tilde{f}_c(n) + z_1 + \sum_{i = 2}^n z_i,
  \end{equation*}
  $z_1$ is a Bernoulli random variable with mean $\frac{2 (P - 1)}{P
    (P - 1) - 1}$, and $z_2, \ldots, z_n$ are iid Bernoulli random
  variables with mean $\frac{2}{P (P - 1) - 1}$. Thus,
  \begin{multline*}
    \expectation f_s(n) = \tilde{f}_s(n) + \frac{2 (P - 1) + 2 (n -
      1)}{P (P - 1) - 1} = 300 (P - 1) - 2 (P - 3) + 2 - 2 (P - 1) (n
    - 1) + \frac{2 (P - 1) + 2 (n - 1)}{P (P - 1) - 1} \\
    = 300 (P - 1) - 2 (P - 3) + 2 + \frac{2 (P - 1)}{P (P - 1) - 1} +
    (n - 1) \paren{\frac{2}{P (P - 1) - 1} - 2 (P - 1)},
  \end{multline*}
  while
  \begin{multline*}
    \expectation f_c(n) = \tilde{f}_c(n) + \frac{2 (P - 1) + 2 (n -
      1)}{P (P - 1) - 1} = 300 (P - 1) - 3 (P - 2) - 2 (P - 2) (n
    - 1) + \frac{2 (P - 1) + 2 (n - 1)}{P (P - 1) - 1} \\
    = 300 (P - 1) - 3 (P - 2) + \frac{2 (P - 1)}{P (P - 1) - 1} +
    (n - 1) \paren{\frac{2}{P (P - 1) - 1} - 2 (P - 2)}.
  \end{multline*}
  Since both are linear functions of $n$, simple arithmetic shows that
  the slackers will reach zero food before the cooperating
  players. Once this happens all slackers will be eliminated, and then
  the cooperating players will see their food increase each round as
  they always get the communal bonus.
\end{proof}

Thus, always slacking is clearly a sub-optimal strategy, although it
is important for any strategy to be able to defeat slackers.

\subsection{Threshold}

Our proposed strategy involves fixing some constant $r \in \sbrace{0,
  1}$ and always hunting with players of reputation $r$ or greater and
always slacking against players of reputation less than $r$. This
strategy behaves well in the extreme cases of very industrious and
very lazy populations; if most players are slacking, i.e.\ they have
low reputation, our strategy will slack against them, while if most
players are cooperating, i.e.\ they have high reputation, our strategy
will cooperate with them. Thus, this strategy will beat out a
population of slackers as in our example above, assuming some player
eventually has a reputation exceeding $r$.

\subsection{Percentile}

A percentile player is identical to a threshold player, except each
round the threshold is chosen to match that of a given percentile of
the player reputations; that is, each round a percentile player slacks
against the bottom $p$ percent of players while hunting with the top
$1 - p$ percent of players. Percentile players exhibit similar
behavior to threshold players at the extremes --- slacking players
will always be slacked against, while cooperating players will always
be cooperated with. The advantage of a percentile strategy is that the
threshold evolves as the game goes on; choosing a correct threshold
value is difficult or impossible without knowing what kind of
opponents you will be against, while choosing a threshold based off
the actual spread of reputations allows for more versatile play.

\section{Code}
\label{sec:code}

We choose to compete with a percentile strategy, where we slack
against the bottom $10\%$ of players. The code is listed below for
completeness.

To arrive at this solution, we created a tournament simulator and
created a number of candidate strategies. We ran many tournaments
involving these players to test different strategies against each
other; through these trials we determined empirically that a
percentile of $.1$ is the most competitive. This code is available via
Git at \url{bitbucket.org/prsteele/brilliant-hunger}.

\lstinputlisting[language=Python]{../submission.py}

\end{document}
