class Player(object):
    """
    This class represents players in the game.

    The functions hunt_choices, hunt_outcomes, round_end, and __init__
    are the public API that will be used to run a tournament. This
    class logs all information passed in from these functions
    (excluding __init__), and so these functions should not be
    overwritten; to inherit from Player, implement the _hunt_choices,
    _hunt_outcomes, and _round_end methods, which are called by the
    public API functions.

    """

    HUNT = 'h'
    SLACK = 's'
    
    def __init__(self):
        """
        Creates a new Player object.

        """

        self.current_food = 0
        self.current_reputation = 0

        # A list of the food the Player has at the start of each round
        self.food = []

        # A list of the reputation of the Player at the start of each round
        self.reputation = []

        # The most recent round the player has participated in
        self.age = 0

        # The number of hunts and slacks executed
        self.hunts = 0
        self.slacks = 0

    def hunt_choices(self, round_number, current_food, current_reputation, m,
                     player_reputations):
        """
        Called to ask the Player to make decisions for a round of play.

        Arguments:
        
        round_number The round number
        current_food The amount of food the Player has
        current_reputation The current reputation of the Player
        m The threshold for cooperation this round
        player_reputations A list of player reputations of all players
                           left in the game.

        Returns:

        A list of Player.HUNT and Player.SLACK entries, where 'h'
        means hunt and 's' means slack. The order of the list
        corresponds to the order of player_reputations.

        """

        # Log the arguments
        self.age = round_number
        self.current_food = current_food
        self.current_reputation = current_reputation
        self.food.append(current_food)
        self.reputation.append(current_reputation)

        choices = self._hunt_choices(round_number, current_food, 
                                  current_reputation, m, player_reputations)

        self.hunts += choices.count(self.HUNT)
        self.slacks += choices.count(self.SLACK)

        # Execute logic
        return choices

    def _hunt_choices(self, round_number, current_food, current_reputation, m,
                     player_reputations):
        """
        Called by hunt_choices to ask the Player to make decisions for
        a round of play.

        Arguments:
        
        round_number The round number
        current_food The amount of food the Player has
        current_reputation The current reputation of the Player
        m The threshold for cooperation this round
        player_reputations A list of player reputations of all players
                           left in the game.

        Returns:

        A list of Player.HUNT and Player.SLACK entries, where 'h'
        means hunt and 's' means slack. The order of the list
        corresponds to the order of player_reputations.

        """

        pass

    def hunt_outcomes(self, food_earnings):
        """
        Called to inform the Player of the outcome of the round.

        Arguments:
        
        food_earnings A list of the food earned from hunting or
                      slacking with an opponent; the order of
                      opponents is the same as the previous order of
                      player_reputations.

        Returns:

        None

        """

        return self._hunt_outcomes(food_earnings)

    def _hunt_outcomes(self, food_earnings):
        """
        Called to inform the Player of the outcome of the round.

        Arguments:
        
        food_earnings A list of the food earned from hunting or
                      slacking with an opponent; the order of
                      opponents is the same as the previous order of
                      player_reputations.

        Returns:

        None

        """

        # Update current food
        self.current_food += sum(food_earnings)

    def round_end(self, award, m, number_hunters):
        """
        Called to inform the Player of the status at the end of the round.

        Arguments:

        award The amount awarded for group cooperation
        m The threshold used for awarding a bonus
        number_hunters The number of total hunts performed in the round

        Returns:

        None

        """

        # Update current food
        self.current_food += award

        return self._round_end(award, m, number_hunters)

    def _round_end(self, award, m, number_hunters):
        """
        Called to inform the Player of the status at the end of the round.

        Arguments:

        award The amount awarded for group cooperation
        m The threshold used for awarding a bonus
        number_hunters The number of total hunts performed in the round

        Returns:

        None

        """

        pass

    def __str__(self):
        """
        Return a string representation of the Player.

        """

        return "%s: %i, %.4f" % \
            (self.__class__.__name__, self.current_food, self.current_reputation)
