from __future__ import division

import random
from itertools import izip

from player import Player

class Tournament(object):
    """
    This class represents a tournament of some number of Player objects.

    """

    # The payoff matrix
    payoff = {Player.HUNT: {Player.HUNT: 0,
                            Player.SLACK: -3},
              Player.SLACK: {Player.HUNT: 1,
                            Player.SLACK: -2}}
              

    def __init__(self, players, M, p, rand):
        """
        Construct a new Tournament with the given players `players'
        that will run for at least `M' rounds; each round after the
        Mth round will have probability `p' of being the last round.
        
        """

        self.players = list(players)
        self.M = M
        self.p = p
        self.rand = rand
        
        starting_food = 300 * (len(players) - 1)
        self.food = {p: starting_food for p in self.players}
        self.hunts = {p: 0 for p in self.players}
        self.slacks = {p: 0 for p in self.players}
        self.awards = 0

    def reputation(self, p):
        """
        Computes the reputation of player p.

        """

        h = self.hunts[p]
        s = self.slacks[p]

        if h > 0 or s > 0:
            return h / (s + h)
        else:
            return 0

    def play(self):
        """
        Run the tournament until completion.

        """

        self.round = 1

        while True:

            # Check if the tournament ends by rounds
            if self.round > self.M and self.rand.random() <= self.p:
                break

            # Check if there are one or fewer players
            if len(self.players) <= 1:
                break
            
            self.play_round()
            self.round += 1
            
    def play_round(self):
        """
        Plays a round of the tournament.

        """

        # Shuffle the player ids and choose a cooperation threshold
        self.shuffle_players()
        threshold = self.choose_threshold()

        opponents = {p: [q for q in self.players if q != p]
                     for p in self.players}

        # Query player actions
        choices = {p: {q: c for (q, c) in
                       izip(opponents[p],
                            p.hunt_choices(self.round,
                                           self.food[p],
                                           self.reputation(p),
                                           threshold,
                                           [self.reputation(q) for q in opponents[p]]))}
                   for p in self.players}
        
        payoffs = {p: {q: self.payoff[choices[p][q]][choices[q][p]]
                       for q in opponents[p]}
                   for p in self.players}

        # Compute communal award
        award = 0
        hunts = sum([v.values().count(Player.HUNT)
                     for (k, v) in choices.items()])
        if hunts >= threshold:
            award = 2 * (len(self.players) - 1)
            self.awards += 1

        # Inform players of the outcome of the hunts and communal
        # award, and update player data
        for p in self.players:
            p.hunt_outcomes([payoffs[p][q] for q in opponents[p]])
            p.round_end(award, threshold, hunts)

            self.food[p] += award + sum(payoffs[p].values())
            self.hunts[p] += choices[p].values().count(Player.HUNT)
            self.slacks[p] += choices[p].values().count(Player.SLACK)

        # Remove players with zero or less food
        self.players = [p for p in self.players if self.food[p] > 0]

    def shuffle_players(self):
        """
        Shuffle the players via a Fisher-Yates shuffle.

        """

        n = len(self.players)
        for i in xrange(0, n):
            j = self.rand.randint(i, n - 1)
            self.players[i], self.players[j] = self.players[j], self.players[i]

    def choose_threshold(self):
        """
        Choose a cooperation threshold.

        """
        
        n = len(self.players)
        return self.rand.randint(1, n * (n - 1))
