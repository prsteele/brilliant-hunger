from __future__ import division

from player import Player

class ReputablePlayer(Player):
    """
    This class represents a Player that will try to maintain a high
    reputation while surviving. The primary goal is to hunt with other
    reputable players to boost reputation while minimizing food loss,
    and slacking against known slackers. Eventually we can start
    slacking more against reputable players if we have reputation to
    burn.

    """

    def __init__(self, rand):
        """
        Constructs a new ReputablePlayer using `rand' as a source of
        random numbers.

        """

        self.rand = rand

        super(ReputablePlayer, self).__init__()

    def _hunt_choices(self, round_number, current_food, current_reputation, m,
                     player_reputations):

        P = len(player_reputations) + 1

        mean_reputation = (current_reputation + sum(player_reputations)) / P

        # Compute the target reputation
        x = mean_reputation * (self.hunts + self.slacks + P - 1) - self.hunts

        # As an edge case, assume a mean reputation of 1/2 to start
        if round_number == 1:
            x = .5

        if x < 0:
            x = 0
        elif x > P - 1:
            x = P - 1

        x = round(x)

        # Distribute x hunts over the players; we should hunt with the
        # most reputable players to minimize the chances of being
        # slacked against when hunting. To begin, we pair each
        # reputation with a unique id.
        reps = zip(player_reputations, xrange(0, len(player_reputations)))

        # Sort the reputations, keeping track of the original positions
        choices = []
        hunts = 0
        for (r, pid) in sorted(reps, key=lambda (a, b): a, reverse=True):
            if hunts < x and (r > 0 or round_number == 1):
                choices.append((Player.HUNT, pid))
                hunts += 1
            else:
                choices.append((Player.SLACK, pid))

            

        return [choice for (choice, pid) in sorted(choices, key=lambda (a, b): b)]
        
