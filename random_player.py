import random
from player import Player

class RandomPlayer(Player):
    """
    A Player who chooses to hunt or slack based on a weighted coin
    flip.

    """

    def __init__(self, p, rand):
        """
        Constructs a new RandomPlayer who will hunt with probability p
        and uses the random.Random instance `rand' to make decisions.

        """

        super(RandomPlayer, self).__init__()
        
        self.p = p
        self.rand = rand

    def _hunt_choices(self, round_number, current_food, current_reputation, m,
                      player_reputations):
        return [Player.HUNT if self.rand.random() <= self.p else Player.SLACK
                for q in player_reputations]

    def __str__(self):
        """
        Return a string representation of the Player.

        """

        return "RandomPlayer(%.2f): %i, %.2f" % \
            (self.p, self.current_food, self.current_reputation)
