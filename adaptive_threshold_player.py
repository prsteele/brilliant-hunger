from player import Player

class AdaptiveThresholdPlayer(Player):
    """
    An AdaptiveThresholdPlayer will always slack against opponents
    below some threshold and always cooperate with those above some
    threshold; this threshold adjusts over rounds.

    """

    def __init__(self, rand, threshold, scale):
        """
        Creates a new AdapativeThresholdPlayer.

        """

        self.rand = rand
        self.threshold = threshold
        self.scale = scale

        super(AdaptiveThresholdPlayer, self).__init__()

    def _hunt_choices(self, round_number, current_food, current_reputation, m,
                      player_reputations):

        if round_number > 10:
            #self.threshold = self.scale * sum(player_reputations) / len(player_reputations)
            self.threshold = self.percentile(player_reputations, self.scale)

        # median = self.percentile(player_reputations, .5)
        # additional_slacks = 0
        # additional_hunts = 0
        
        # choices = []
        # for r in player_reputations:
        #     if current_reputation > median and r > current_reputation and self.rand.random() <= r:
        #         choices.append(Player.SLACK)
        #         additional_slacks += 1
        #     elif r >= self.threshold:
        #         choices.append(Player.HUNT)
        #         additional_hunts += 1
        #     else:
        #         choices.append(Player.SLACK)
        #         additional_slacks += 1

        #     current_reputation = (self.hunts + additional_hunts) / \
        #         (self.hunts + self.slacks + additional_slacks + additional_hunts)

        # return choices

        return [Player.HUNT if r >= self.threshold else Player.SLACK
                for r in player_reputations]

    def percentile(self, li, p):
        li = sorted(li)

        return li[int((len(li) - 1) * p)]

    def __str__(self):
        """
        Return a string representation of the Player.

        """

        return "AdaptiveThresholdPlayer(%.2f): %i, %.2f" % \
            (self.scale, self.current_food, self.current_reputation)
