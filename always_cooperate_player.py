from player import Player

class AlwaysCooperatePlayer(Player):
    """
    A Player that always chooses to cooperate with opponents.

    """

    def _hunt_choices(self, round_number, current_food, current_reputation, m,
                     player_reputations):
        return [self.HUNT] * len(player_reputations)

    def __str__(self):
        """
        Return a string representation of the Player.

        """

        return "AlwaysCooperatePlayer: %i, %.2f" % \
            (self.current_food, self.current_reputation)
