"""
Copyright 2013 Patrick Steele.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

class Player(object):

    def __init__(self):
        self.percentile = .1
        self.HUNT = 'h'
        self.SLACK = 's'
    
    def hunt_choices(self, round_number, current_food, current_reputation,
                     m,  player_reputations):
        threshold = self._percentile(player_reputations, self.percentile)

        return [self.HUNT if r >= threshold else self.SLACK
                for r in player_reputations]

    def hunt_outcomes(self, food_earnings):
        pass

    def round_end(self, award, m, number_hunters):
        pass

    def _percentile(self, li, p):
        """
        Returns the pth percentile of a list.

        """

        li = sorted(li)

        return li[int((len(li) - 1) * p)]
